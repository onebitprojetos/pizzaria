﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;


namespace PDFiTextSharp
{
    class GerarPdf
    {
        public void CriarArquivo()
        {
            string produto, qtd, valor;
            produto = "PPPPPPPPPP   ";
            qtd = " 0,250";
            valor = "  1000,00";

            StreamWriter arquivo = new StreamWriter("C:\\Users\\SauloJCF\\Documents\\MyFile.txt");
            arquivo.WriteLine("Pizzaria");
            arquivo.Write(produto);
            arquivo.Write(qtd);
            arquivo.Write(valor);
            arquivo.WriteLine();
            arquivo.WriteLine();
            arquivo.WriteLine();

            arquivo.Close();


        }

        public void geradorPdf(string id_pedido, string dinheiro, string troco)
        {

            NovaPizzaria.Conexao conexao = new NovaPizzaria.Conexao();
            if (troco == "")
            {
                troco = "0,00";
            }
            // os comandos slqlite são jogados dentro de strings
            string clienteSelect = "select cliente from pedido where id = " + id_pedido + ";";
            string diaSelect = "select data from pedido where id =" + id_pedido + ";";
            string totalSelect = "select total " +
                "from pedido where id = " + id_pedido + "; ";
            string descontoSelect = "select desconto " +
                "from pedido where id = " + id_pedido + "; ";
            string subtotalSelect = "select subtotal " +
                "from pedido where id = " + id_pedido + "; ";

            string quantidadeTotal = "select sum(quantidade) from itens_pedido where id_pedido=" + id_pedido + ";";

            string produtosSelect = "select pdt.nome,ip.quantidade,pdt.valor " +
                "from pedido p inner join itens_pedido ip on p.id = ip.id_pedido " +
                "inner join produto pdt on ip.id_produto = pdt.id " +
                "where p.id = " + id_pedido + "; ";
            conexao.conectar();
            // os comandos são executados e o resultado é jogado dentro das respectivas variáveis cmd
            try
            {
                SQLiteCommand cmdDia = new SQLiteCommand(diaSelect, conexao.conectar());
                SQLiteCommand cmdCli = new SQLiteCommand(clienteSelect, conexao.conectar());
                SQLiteCommand cmdTot = new SQLiteCommand(totalSelect, conexao.conectar());
                SQLiteCommand cmdDes = new SQLiteCommand(descontoSelect, conexao.conectar());
                SQLiteCommand cmdSubT = new SQLiteCommand(subtotalSelect, conexao.conectar());
                SQLiteCommand cmdQuant = new SQLiteCommand(quantidadeTotal, conexao.conectar());
                SQLiteCommand cmdProd = new SQLiteCommand(produtosSelect, conexao.conectar());
                //texto do cabeçalho
                string textoCabecalho = "CNPJ: 08.098.307/0001-89\nRUA JOÃO BATISTA MAGALHÃES 415 - PAQUETÁ" +
                    "\nVIRGINÓPOLIS - CEP: 39730-000\n" +
                    "COMPROVANTE DE VENDA\n" +
                    "-------------------------\n" +
                    "SEM VALOR FISCAL\n";

                //Os comandos que retornam um único resultado são convertidos e armazenados dentro de sua respectiva string
                dinheiro = $"{Convert.ToDouble(dinheiro):F2}";
                troco = $"{Convert.ToDouble(troco):F2}";
                string data = Convert.ToString(cmdDia.ExecuteScalar());
                string nomeDoCliente = Convert.ToString(cmdCli.ExecuteScalar());
                string total = Convert.ToString(cmdTot.ExecuteScalar());
                total = $"{Convert.ToDouble(total):F2}";
                string desconto = Convert.ToString(cmdDes.ExecuteScalar());
                desconto = $"{Convert.ToDouble(desconto):F2}";
                string subTotal = Convert.ToString(cmdSubT.ExecuteScalar());
                subTotal = $"{Convert.ToDouble(subTotal):F2}";
                string quantidade = Convert.ToString(cmdQuant.ExecuteScalar());

                // pagamento = pagamento.ToString("C");

                PdfPTable table = new PdfPTable(3);//cria uma tabela
                table.DefaultCell.Border = Rectangle.NO_BORDER;//retira as bordas

                Font fontH1 = new Font(Font.NORMAL, 9);//formatação

                //cabeçalho
                table.AddCell(new Phrase("PRODUTO", fontH1));
                table.AddCell(new Phrase("QTD", fontH1));
                table.AddCell(new Phrase("VALOR", fontH1));

                //leitura do resultado de uma consulta que retorna mais de uma linha e coluna
                SQLiteDataReader sQLiteDataReader = cmdProd.ExecuteReader();
                /*
                string produtoo, qtd, valor;
                produtoo = "PPPPPPPPPP   ";//14


                qtd = " 00";//3
                valor = "  1000,00";//9

                StreamWriter arquivo = new StreamWriter("C:\\Users\\SauloJCF\\Documents\\MyFile.txt");
                arquivo.WriteLine("Pizzaria");
                String s = String.Format("{0,-10} {1,-5} {2,-5}\n\n", "Produto", "Qtd", "Valor");
                
              //  arquivo.Write(s);
                //arquivo.Write(qtd);
                //arquivo.Write(valor);
               // arquivo.WriteLine();

                //arquivo.Close();
                //--
                */
                while (sQLiteDataReader.Read())
                {
                    //cada resultado é jogado dentro de uma coluna, e a cada iteração cria-se uma nova linha
                    table.AddCell((new Phrase(((IDataRecord)sQLiteDataReader)[0].ToString(), fontH1)));
                    table.AddCell((new Phrase(((IDataRecord)sQLiteDataReader)[1].ToString(), fontH1)));
                    table.AddCell((new Phrase($"{ Convert.ToDouble(((IDataRecord)sQLiteDataReader)[2].ToString()):F2}", fontH1)));
                    //teste
                    //produtoo = sQLiteDataReader[0].ToString();
                   // qtd = sQLiteDataReader[1].ToString();
                    //valor = sQLiteDataReader[2].ToString();
                   // s = "";
                     //s = String.Format("{0,-15} {1,-5} {2,-5}\n", "oo\noooooooooo", qtd, valor);
                    
                   // arquivo.Write(s);
                }
                //arquivo.Close();
                //criação de uma nova tabela para formatar o rodapé
                PdfPTable tabela = new PdfPTable(2);
                tabela.DefaultCell.Border = Rectangle.NO_BORDER;
                tabela.AddCell(new Phrase("QTD. ITENS:", fontH1));
                //tabela.AddCell(new Phrase(" ", fontH1));
                tabela.AddCell(new Phrase(" \n" + quantidade, fontH1));
                tabela.AddCell(new Phrase("VALOR TOTAL:", fontH1));
                //tabela.AddCell(new Phrase(" ", fontH1));
                tabela.AddCell(new Phrase("\n" + subTotal, fontH1));
                tabela.AddCell(new Phrase("PARA PAGAMEN- TOS EM DINHE-IRO:", fontH1));
                //tabela.AddCell(new Phrase(" ", fontH1));
                tabela.AddCell(new Phrase("\n\n\n\n" + total, fontH1));
                /*
                tabela.AddCell(new Phrase("VALOR PAGO:", fontH1));
                tabela.AddCell(new Phrase(" ", fontH1));
                tabela.AddCell(new Phrase(dinheiro, fontH1));
                tabela.AddCell(new Phrase("TROCO:", fontH1));
                tabela.AddCell(new Phrase(" ", fontH1));
                tabela.AddCell(new Phrase(troco, fontH1));
                */
                //criação do documento pdf (doc)
                Document doc = new Document(PageSize.A8);
                doc.SetMargins(15, 0, 10, 10);
                doc.AddCreationDate();

                // local de criãção
                string caminho = @"relatorio.pdf";

                PdfWriter writer = PdfWriter.GetInstance(doc, new

                FileStream(caminho, FileMode.Create));

                doc.Open();//o documento é aberto

                // criação de elementos paragraph, de acordo com sua formatação
                Paragraph nome = new Paragraph("CHALÉ PIZZARIA", new Font(Font.NORMAL, 9, Font.BOLD));
                nome.Alignment = Element.ALIGN_CENTER;

                Paragraph cabecalho = new Paragraph("", new Font(Font.NORMAL, 7));
                cabecalho.Alignment = Element.ALIGN_CENTER;


                Paragraph tituloCliente = new Paragraph("CLIENTE: " + nomeDoCliente, new Font(Font.NORMAL, 9, Font.BOLD));
                tituloCliente.Alignment = Element.ALIGN_CENTER;


                doc.Add(nome);// nome da pizzaria é adicionado ao pdf

                cabecalho.Add(textoCabecalho); // o texto é adicionado ao elemento cabeçalho

                doc.Add(cabecalho);// o cabeçalho é adicionado ao pdf

                // adicionada a tabela com os produtos
                Paragraph produto = new Paragraph("", new Font(Font.NORMAL, 9));
                produto.Alignment = Element.ALIGN_JUSTIFIED;


                produto.Add(table);
                produto.Add("       RESUMO");
                doc.Add(produto);


                //formatção do rodapé
                Paragraph rodape = new Paragraph("", new Font(Font.NORMAL, 7));
                rodape.Alignment = Element.ALIGN_CENTER;

                Paragraph final = new Paragraph(data + "\n-------------------------\nwww.onebitjr.com.br\n + 55 33 98832 - 2418", new Font(Font.NORMAL, 7));
                final.Alignment = Element.ALIGN_CENTER;

                //adicionando o texto ao rodapé
                rodape.Add(tabela);
                rodape.Add(tituloCliente);



                doc.Add(rodape);//inserção do rodapé no pdf
                doc.Add(final);
                doc.Close();
                conexao.desconectar();
                System.Diagnostics.Process.Start(caminho);

               // FormTelaImpressao imp = new FormTelaImpressao();
               // imp.Show();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                
            }
            

            
        }
     
    }
}