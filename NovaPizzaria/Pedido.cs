﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;
using System.Globalization;

namespace NovaPizzaria
{
    public class Pedido
    {
        Conexao conexao = new Conexao();
        SQLiteCommand cmd = new SQLiteCommand();
        public String Mensagem;
        //Metodo para Cadastrar um novo pedido
        public void Cadastrar(String mesa, String cliente)
        {
            cmd.CommandText = "INSERT INTO pedido (cliente,mesa, data) VALUES (@cliente, @mesa, @data)";

            cmd.Parameters.AddWithValue("@cliente", cliente);
            cmd.Parameters.AddWithValue("@mesa", mesa);
            cmd.Parameters.AddWithValue("@data", DateTime.Now);
           
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }
        public int ObterItem()
        {
            int id_lock;
            Conexao conexao = new Conexao();
            SQLiteCommand cmd = new SQLiteCommand();
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.CommandText = "select max(id) from pedido";
                if (Convert.ToString(cmd.ExecuteScalar()) == "")
                {
                    id_lock = 1;
                }
                else
                {
                    id_lock = (int)Convert.ToInt16(cmd.ExecuteScalar());
                }
                conexao.desconectar();
                return id_lock;
            }
            catch(Exception e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
            return 0;
        }

       
        public void FinalizaPedido(decimal subtotal,decimal desconto,decimal total,int id)
        {
            String subtotalPonto = subtotal.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            String descontoPonto = desconto.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            String totalPonto = total.ToString(CultureInfo.CreateSpecificCulture("en-US"));
  
            cmd.CommandText = "update pedido set subtotal=@subtotal,desconto=@desconto,total=@total where id=@id";
            cmd.Parameters.AddWithValue("@subtotal", subtotalPonto);
            cmd.Parameters.AddWithValue("@desconto", descontoPonto);
            cmd.Parameters.AddWithValue("@total", totalPonto);
            cmd.Parameters.AddWithValue("@id", id);
            
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }





        //Metodo para Excluir Pedido
        public void ExcluirPedido(int idcategoria)
        {
            cmd.CommandText = "DELETE FROM pedido WHERE id=@idpedido";
            cmd.Parameters.AddWithValue("@idpedido", idcategoria);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Excluído com sucesso!","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }

        public DataTable ListaPedido()
        {
            Conexao conexao = new Conexao();
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "select id,data,cliente,mesa,subtotal,desconto,total from pedido order by id desc;";
            try
            {
                SQLiteDataAdapter obj = new SQLiteDataAdapter(cmd.CommandText, conexao.conectar());
                DataTable dtlista = new DataTable();
                obj.Fill(dtlista);
                conexao.desconectar();
                return dtlista;

            }
            catch (SQLiteException erro)
            {
                throw erro;
            }

        }
        public string GetTotalDia()
        {
            String data = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day +" 00:00:00";
            try
            {
                String select = "select sum(total) from pedido where data>= datetime('"+ data +"');";
                SQLiteCommand cmdTotal = new SQLiteCommand(select, conexao.conectar());
                string total = Convert.ToString(cmdTotal.ExecuteScalar());
                if (total == "")
                {
                    total = "0";
                }
                return (total);
            }catch(Exception e)
            {
                return ("");
            }
            
        }


        public void Editar(int id, int mesa,String cliente,String pagamento, decimal total, decimal subTotal, decimal desconto)
        {
            cmd.CommandText = "update pedido set mesa=@mesa, cliente=@cliente,forma_pagamento=@pagamento,total=@total,subtotal=@subTotal,desconto=@desconto where id=@id";
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@mesa", mesa);
            cmd.Parameters.AddWithValue("@cliente", cliente);
            cmd.Parameters.AddWithValue("@pagamento", pagamento);
            cmd.Parameters.AddWithValue("@total", total);
            cmd.Parameters.AddWithValue("@subTotal", subTotal);      
            cmd.Parameters.AddWithValue("@desconto", desconto);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Alterado com Sucesso","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }

    }
}
