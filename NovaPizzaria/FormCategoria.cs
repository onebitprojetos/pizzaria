﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace NovaPizzaria
{
    public partial class FormCategoria : Form
    {
        public FormCategoria()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CamposVazios(this))
            {
            }
            else
            {
                Categoria c = new Categoria();
                c.Cadastrar(textBoxNomeCategoria.Text, textBoxDescCategoria.Text);
                textBoxNomeCategoria.Clear();
                textBoxDescCategoria.Clear();
                textBoxNomeCategoria.Focus();
            }
        }

        private void FormCategoria_Load(object sender, EventArgs e)
        {
        }
        public bool CamposVazios(Control _ctrl)
        {

            if (textBoxNomeCategoria.Text == "")
            {
                MessageBox.Show("Entre com o nome da Categoria.", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxNomeCategoria.Focus();
                return true;
            }
            return false;

        }

        private void textBoxDescCategoria_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
