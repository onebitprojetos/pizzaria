﻿namespace NovaPizzaria
{
    partial class FormMesa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMesa));
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxMesaPedido = new System.Windows.Forms.TextBox();
            this.Pedido = new System.Windows.Forms.GroupBox();
            this.Atualizar = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.busca = new System.Windows.Forms.TextBox();
            this.ProdutosPedido = new System.Windows.Forms.Label();
            this.produtosCadastrados = new System.Windows.Forms.Label();
            this.ocultarForm = new System.Windows.Forms.Button();
            this.remover = new System.Windows.Forms.Button();
            this.adicionar = new System.Windows.Forms.Button();
            this.itensPed = new System.Windows.Forms.ListView();
            this.prodId = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.prodCod = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.prodNome = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.prodPreco = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.QTD = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.todosProd = new System.Windows.Forms.ListView();
            this.idProd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.codProd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.nomProd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.precProd = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.totalPed = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.idPedido = new System.Windows.Forms.TextBox();
            this.nomeCliente = new System.Windows.Forms.TextBox();
            this.cancelar = new System.Windows.Forms.Button();
            this.prossFormaPagamento = new System.Windows.Forms.Button();
            this.novoPedido = new System.Windows.Forms.Button();
            this.Pedido.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(534, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(286, 31);
            this.label3.TabIndex = 14;
            this.label3.Text = "Cadastro de Pedidos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(294, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 24);
            this.label2.TabIndex = 13;
            this.label2.Text = "Nome do Cliente:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(294, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Mesa:";
            // 
            // textBoxMesaPedido
            // 
            this.textBoxMesaPedido.Enabled = false;
            this.textBoxMesaPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxMesaPedido.Location = new System.Drawing.Point(366, 94);
            this.textBoxMesaPedido.Name = "textBoxMesaPedido";
            this.textBoxMesaPedido.Size = new System.Drawing.Size(62, 26);
            this.textBoxMesaPedido.TabIndex = 10;
            // 
            // Pedido
            // 
            this.Pedido.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Pedido.Controls.Add(this.Atualizar);
            this.Pedido.Controls.Add(this.label4);
            this.Pedido.Controls.Add(this.busca);
            this.Pedido.Controls.Add(this.ProdutosPedido);
            this.Pedido.Controls.Add(this.produtosCadastrados);
            this.Pedido.Controls.Add(this.ocultarForm);
            this.Pedido.Controls.Add(this.remover);
            this.Pedido.Controls.Add(this.adicionar);
            this.Pedido.Controls.Add(this.itensPed);
            this.Pedido.Controls.Add(this.todosProd);
            this.Pedido.Controls.Add(this.prossFormaPagamento);
            this.Pedido.Controls.Add(this.totalPed);
            this.Pedido.Controls.Add(this.label7);
            this.Pedido.Location = new System.Drawing.Point(0, 121);
            this.Pedido.Name = "Pedido";
            this.Pedido.Size = new System.Drawing.Size(1215, 522);
            this.Pedido.TabIndex = 15;
            this.Pedido.TabStop = false;
            this.Pedido.Visible = false;
            this.Pedido.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // Atualizar
            // 
            this.Atualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Atualizar.Location = new System.Drawing.Point(426, 74);
            this.Atualizar.Name = "Atualizar";
            this.Atualizar.Size = new System.Drawing.Size(75, 31);
            this.Atualizar.TabIndex = 34;
            this.Atualizar.Text = "Atualizar";
            this.Atualizar.UseVisualStyleBackColor = true;
            this.Atualizar.Click += new System.EventHandler(this.Atualizar_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 24);
            this.label4.TabIndex = 33;
            this.label4.Text = "Busca:";
            // 
            // busca
            // 
            this.busca.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.busca.Location = new System.Drawing.Point(110, 76);
            this.busca.Name = "busca";
            this.busca.Size = new System.Drawing.Size(310, 26);
            this.busca.TabIndex = 30;
            this.busca.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // ProdutosPedido
            // 
            this.ProdutosPedido.AutoSize = true;
            this.ProdutosPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProdutosPedido.Location = new System.Drawing.Point(662, 43);
            this.ProdutosPedido.Name = "ProdutosPedido";
            this.ProdutosPedido.Size = new System.Drawing.Size(195, 24);
            this.ProdutosPedido.TabIndex = 32;
            this.ProdutosPedido.Text = "Produtos do Pedido";
            // 
            // produtosCadastrados
            // 
            this.produtosCadastrados.AutoSize = true;
            this.produtosCadastrados.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.produtosCadastrados.Location = new System.Drawing.Point(36, 43);
            this.produtosCadastrados.Name = "produtosCadastrados";
            this.produtosCadastrados.Size = new System.Drawing.Size(214, 24);
            this.produtosCadastrados.TabIndex = 31;
            this.produtosCadastrados.Text = "Produtos Cadastrados";
            // 
            // ocultarForm
            // 
            this.ocultarForm.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ocultarForm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ocultarForm.Location = new System.Drawing.Point(507, 246);
            this.ocultarForm.Name = "ocultarForm";
            this.ocultarForm.Size = new System.Drawing.Size(153, 33);
            this.ocultarForm.TabIndex = 25;
            this.ocultarForm.Text = "Ocultar Mesa";
            this.ocultarForm.UseVisualStyleBackColor = true;
            this.ocultarForm.Click += new System.EventHandler(this.button5_Click);
            // 
            // remover
            // 
            this.remover.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.remover.Location = new System.Drawing.Point(507, 207);
            this.remover.Name = "remover";
            this.remover.Size = new System.Drawing.Size(153, 33);
            this.remover.TabIndex = 24;
            this.remover.Text = "<< Remover";
            this.remover.UseVisualStyleBackColor = true;
            this.remover.Click += new System.EventHandler(this.remover_Click);
            // 
            // adicionar
            // 
            this.adicionar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adicionar.Location = new System.Drawing.Point(507, 168);
            this.adicionar.Name = "adicionar";
            this.adicionar.Size = new System.Drawing.Size(153, 33);
            this.adicionar.TabIndex = 23;
            this.adicionar.Text = "Adicionar >>";
            this.adicionar.UseVisualStyleBackColor = true;
            this.adicionar.Click += new System.EventHandler(this.adicionar_Click);
            // 
            // itensPed
            // 
            this.itensPed.AllowColumnReorder = true;
            this.itensPed.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.prodId,
            this.prodCod,
            this.prodNome,
            this.prodPreco,
            this.QTD});
            this.itensPed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.itensPed.FullRowSelect = true;
            this.itensPed.GridLines = true;
            this.itensPed.HideSelection = false;
            this.itensPed.Location = new System.Drawing.Point(666, 107);
            this.itensPed.Name = "itensPed";
            this.itensPed.Size = new System.Drawing.Size(461, 222);
            this.itensPed.TabIndex = 22;
            this.itensPed.UseCompatibleStateImageBehavior = false;
            this.itensPed.View = System.Windows.Forms.View.Details;
            // 
            // prodId
            // 
            this.prodId.Width = 0;
            // 
            // prodCod
            // 
            this.prodCod.Text = "Código";
            this.prodCod.Width = 67;
            // 
            // prodNome
            // 
            this.prodNome.Text = "Nome do Produto";
            this.prodNome.Width = 223;
            // 
            // prodPreco
            // 
            this.prodPreco.Text = "Preço";
            this.prodPreco.Width = 81;
            // 
            // QTD
            // 
            this.QTD.Text = "QTD";
            this.QTD.Width = 86;
            // 
            // todosProd
            // 
            this.todosProd.AllowColumnReorder = true;
            this.todosProd.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idProd,
            this.codProd,
            this.nomProd,
            this.precProd});
            this.todosProd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.todosProd.FullRowSelect = true;
            this.todosProd.GridLines = true;
            this.todosProd.HideSelection = false;
            this.todosProd.Location = new System.Drawing.Point(40, 107);
            this.todosProd.Name = "todosProd";
            this.todosProd.Size = new System.Drawing.Size(461, 222);
            this.todosProd.TabIndex = 21;
            this.todosProd.UseCompatibleStateImageBehavior = false;
            this.todosProd.View = System.Windows.Forms.View.Details;
            this.todosProd.SelectedIndexChanged += new System.EventHandler(this.todosProd_SelectedIndexChanged);
            this.todosProd.DoubleClick += new System.EventHandler(this.adicionar_Click);
            // 
            // idProd
            // 
            this.idProd.DisplayIndex = 3;
            this.idProd.Width = 0;
            // 
            // codProd
            // 
            this.codProd.DisplayIndex = 0;
            this.codProd.Text = "Código";
            this.codProd.Width = 69;
            // 
            // nomProd
            // 
            this.nomProd.DisplayIndex = 1;
            this.nomProd.Text = "Nome do Produto";
            this.nomProd.Width = 314;
            // 
            // precProd
            // 
            this.precProd.DisplayIndex = 2;
            this.precProd.Text = "Preço";
            this.precProd.Width = 73;
            // 
            // totalPed
            // 
            this.totalPed.Enabled = false;
            this.totalPed.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalPed.Location = new System.Drawing.Point(40, 396);
            this.totalPed.Name = "totalPed";
            this.totalPed.Size = new System.Drawing.Size(156, 62);
            this.totalPed.TabIndex = 16;
            this.totalPed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.totalPed.TextChanged += new System.EventHandler(this.totalPed_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(33, 356);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 37);
            this.label7.TabIndex = 16;
            this.label7.Text = "Total";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(534, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 24);
            this.label6.TabIndex = 27;
            this.label6.Text = "Id_Pedido";
            this.label6.Visible = false;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // idPedido
            // 
            this.idPedido.Enabled = false;
            this.idPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.idPedido.Location = new System.Drawing.Point(635, 94);
            this.idPedido.Name = "idPedido";
            this.idPedido.Size = new System.Drawing.Size(42, 26);
            this.idPedido.TabIndex = 26;
            this.idPedido.Visible = false;
            // 
            // nomeCliente
            // 
            this.nomeCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomeCliente.Location = new System.Drawing.Point(473, 55);
            this.nomeCliente.Name = "nomeCliente";
            this.nomeCliente.Size = new System.Drawing.Size(355, 26);
            this.nomeCliente.TabIndex = 29;
            this.nomeCliente.KeyUp += new System.Windows.Forms.KeyEventHandler(this.nomeCliente_KeyUp);
            // 
            // cancelar
            // 
            this.cancelar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelar.Image = global::NovaPizzaria.Properties.Resources._1486564399_close_81512;
            this.cancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cancelar.Location = new System.Drawing.Point(1010, 45);
            this.cancelar.Name = "cancelar";
            this.cancelar.Size = new System.Drawing.Size(160, 46);
            this.cancelar.TabIndex = 2;
            this.cancelar.Text = "Cancelar Pedido";
            this.cancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cancelar.UseVisualStyleBackColor = true;
            this.cancelar.Click += new System.EventHandler(this.buttonCancelar_Click);
            // 
            // prossFormaPagamento
            // 
            this.prossFormaPagamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.prossFormaPagamento.Image = global::NovaPizzaria.Properties.Resources.arrow_double_right_15738;
            this.prossFormaPagamento.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.prossFormaPagamento.Location = new System.Drawing.Point(221, 396);
            this.prossFormaPagamento.Name = "prossFormaPagamento";
            this.prossFormaPagamento.Size = new System.Drawing.Size(161, 62);
            this.prossFormaPagamento.TabIndex = 19;
            this.prossFormaPagamento.Text = "Prosseguir para Pagamento";
            this.prossFormaPagamento.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.prossFormaPagamento.UseVisualStyleBackColor = true;
            this.prossFormaPagamento.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // novoPedido
            // 
            this.novoPedido.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.novoPedido.Image = global::NovaPizzaria.Properties.Resources.add_icon_icons_com_74429__1_;
            this.novoPedido.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.novoPedido.Location = new System.Drawing.Point(844, 45);
            this.novoPedido.Name = "novoPedido";
            this.novoPedido.Size = new System.Drawing.Size(160, 46);
            this.novoPedido.TabIndex = 1;
            this.novoPedido.Text = "Novo Pedido";
            this.novoPedido.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.novoPedido.UseVisualStyleBackColor = true;
            this.novoPedido.Click += new System.EventHandler(this.Button1_Click);
            // 
            // FormMesa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(1188, 749);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.idPedido);
            this.Controls.Add(this.cancelar);
            this.Controls.Add(this.nomeCliente);
            this.Controls.Add(this.Pedido);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxMesaPedido);
            this.Controls.Add(this.novoPedido);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormMesa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormNovoPedido_Load);
            this.Pedido.ResumeLayout(false);
            this.Pedido.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxMesaPedido;
        private System.Windows.Forms.Button novoPedido;
        private System.Windows.Forms.GroupBox Pedido;
        private System.Windows.Forms.TextBox totalPed;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button prossFormaPagamento;
        private System.Windows.Forms.TextBox nomeCliente;
        private System.Windows.Forms.Button cancelar;
        private System.Windows.Forms.ListView todosProd;
        private System.Windows.Forms.ListView itensPed;
        private System.Windows.Forms.ColumnHeader prodNome;
        private System.Windows.Forms.ColumnHeader prodPreco;
        private System.Windows.Forms.ColumnHeader nomProd;
        private System.Windows.Forms.Button remover;
        private System.Windows.Forms.Button adicionar;
        private System.Windows.Forms.ColumnHeader precProd;
        private System.Windows.Forms.Button ocultarForm;
        private System.Windows.Forms.ColumnHeader prodCod;
        private System.Windows.Forms.ColumnHeader codProd;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox idPedido;
        private System.Windows.Forms.Label ProdutosPedido;
        private System.Windows.Forms.Label produtosCadastrados;
        private System.Windows.Forms.ColumnHeader idProd;
        private System.Windows.Forms.ColumnHeader prodId;
        private System.Windows.Forms.TextBox busca;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Atualizar;
        private System.Windows.Forms.ColumnHeader QTD;
    }
}