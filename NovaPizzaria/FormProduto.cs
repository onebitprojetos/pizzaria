﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace NovaPizzaria
{
    public partial class FormProduto : Form
    {
        public FormProduto()
        {
            InitializeComponent();
            if (Convert.ToInt32(this.existeCategoria()) >= 1)
            {
                MostrarCategoria();
            }
            else
            {
                MessageBox.Show("Não existem categorias cadastradas. Cadastre pelo menos uma categoria antes de cadastrar produtos.", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                this.Close();
            }
        }
        private void MostrarCategoria()
        {
            Categoria cat = new Categoria();

            comboBox1.DataSource = new BindingSource(cat.Itens(), "Categoria");
            comboBox1.ValueMember = "id";
            comboBox1.DisplayMember = "nome";
            comboBox1.SelectedIndex = 0;

        }

        public String existeCategoria()
        {
            
            Conexao conexao = new Conexao();
            conexao.conectar();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand();
                String produtosSelect = "Select count() from categoria";
                SQLiteCommand cmdProd = new SQLiteCommand(produtosSelect, conexao.conectar());
                string quant = Convert.ToString(cmdProd.ExecuteScalar());
                conexao.desconectar();
                return quant;
            }
            catch (SQLiteException erro)
            {
                throw erro;
            }

        }

        private void FormProduto_Load(object sender, EventArgs e)
        {
            nome.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!CamposVazios(this))
            {
                Produto novo = new Produto();
                int id = Convert.ToInt32(comboBox1.SelectedValue);
                novo.CadastrarProduto(txtCodigo.Text,nome.Text, unidade.Text, Convert.ToDecimal(valor.Text), descricao.Text, id);
                txtCodigo.Clear();
                nome.Clear();
                unidade.Clear();
                valor.Clear();
                descricao.Clear();
                txtCodigo.Focus();
            }

        }

        private void valor_TextChanged(object sender, EventArgs e)
        {

        }
        public bool CamposVazios(Control _ctrl)
        {
            if(txtCodigo.Text == "")
            {
                MessageBox.Show("Entre com a Código.", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtCodigo.Focus();
                return true;
            }
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Entre com a categoria.", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBox1.Focus();
                return true;
            }
            else if (nome.Text == "")
            {
                MessageBox.Show("Entre com o nome do produto", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                nome.Focus();
                return true;
            }
            else if (valor.Text == "")
            {
                MessageBox.Show("Entre com o preço do produto", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                valor.Focus();
                return true;
            }

            return false;

        }

        private void valor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void nome_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
