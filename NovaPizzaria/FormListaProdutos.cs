﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormListaProdutos : Form
    {
        public FormListaProdutos()
        {
            InitializeComponent();
            listar();
        }

        private void FormListaProdutos_Load(object sender, EventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;

            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.BackgroundColor = Color.White;

            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.Columns[4].DefaultCellStyle.Format = "C";
            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }
        public void listar()
        {
            Produto list = new Produto();
            DataTable dtlist = list.ListaProdutos();
            BindingSource bs = new BindingSource();
            bs.DataSource = dtlist;
            dataGridView1.DataSource = bs;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            else
            {
                if (MessageBox.Show("Tem certeza que deseja Excluir?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Produto p = new Produto();
                    int a,op,cod;
                    a = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    cod = int.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString());
                    if(MessageBox.Show("Deseja retroceder os demais codigos?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        op = 1;
                    }
                    else
                    {
                        op = 0;
                    }
                    p.ExcluirProduto(a,op,cod);
                    if (dataGridView1.RowCount > 0)
                        dataGridView1.Rows[0].Selected = true;
                    listar();
                }
                else
                {
                    MessageBox.Show("Exclusão Cancelada com Sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido");
            else
            {
                int id,codigo;
                String nome, descricao,unidade,categoria;
                decimal valor;
                id = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                codigo = int.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString());
                nome = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                unidade = dataGridView1.CurrentRow.Cells[3].Value.ToString();
                valor = decimal.Parse(dataGridView1.CurrentRow.Cells[4].Value.ToString());
                descricao = dataGridView1.CurrentRow.Cells[5].Value.ToString();
                categoria = dataGridView1.CurrentRow.Cells[6].Value.ToString();
                FormEditarProduto novaForm = new FormEditarProduto(Convert.ToString(id), Convert.ToString(codigo), nome,unidade,Convert.ToString(valor), descricao,categoria);
                novaForm.ShowDialog(this);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
