﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormListarPedidos : Form
    {
        public FormListarPedidos()
        {
            InitializeComponent();
            listar();
        }

        //CHAMAR O MÉTODO LISTAR NA GRID
        
        //função para adicionar itens ao pedido
        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro selecionado");
            else
            {
                int idPedido;
                String cliente;
                idPedido = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                cliente = dataGridView1.CurrentRow.Cells[3].Value.ToString();
               
               
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido");
            else
            {
                if (MessageBox.Show("Tem certeza que deseja Excluir?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Pedido p = new Pedido();
                    int a;
                    a = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    p.ExcluirPedido(a);
                    if (dataGridView1.RowCount > 0)
                        dataGridView1.Rows[0].Selected = true;
                    listar();
                }
                else
                {
                    MessageBox.Show("Exclusão Cancelada com Sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
