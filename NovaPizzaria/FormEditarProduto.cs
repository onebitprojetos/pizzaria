﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormEditarProduto : Form
    {
        private String idProd;
        public String codProd;

        public FormEditarProduto(String id, String codigo,String nome,String unidade,String valor, String descricao,String categoria)
        {
            InitializeComponent();
            this.idProd = id;
            this.codProd = codigo;
            textBox1.Text = nome;
            textBox2.Text = unidade;
            textBox3.Text = valor;
            textBox4.Text = descricao;
            textBox5.Text = codigo;
            textBox6.Text = categoria;
            MostrarCategoria();
           
        }

        private void EditarProduto_Load(object sender, EventArgs e)
        {

        }
        private void MostrarCategoria()
        {
            
            Categoria cat = new Categoria();
            comboBox1.DataSource = new BindingSource(cat.Itens(), "Categoria");
            comboBox1.ValueMember = "id";
            comboBox1.DisplayMember = "nome";
            comboBox1.SelectedIndex = comboBox1.FindStringExact(textBox6.Text);

        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (CamposVazios(this))
            {
            }
            else
            {
                if (MessageBox.Show("Tem certeza que deseja Editar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Produto produto = new Produto();
                    produto.codigoProduto = codProd;
                    int id_cat = Convert.ToInt32(comboBox1.SelectedValue);
                    produto.Editar(textBox1.Text, textBox2.Text, Convert.ToDecimal(textBox3.Text), textBox4.Text,Convert.ToInt32( textBox5.Text),id_cat,Convert.ToInt32(idProd));
                    ((FormListaProdutos)Owner).listar();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Edição Cancelada com Sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public bool CamposVazios(Control _ctrl)
        {
            if (comboBox1.Text == "")
            {
                MessageBox.Show("Entre com a categoria", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                comboBox1.Focus();
                return true;
            }else if (textBox1.Text=="")
            {
                MessageBox.Show("Entre com o nome do produto", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox1.Focus();
                return true;
            }
            else if (textBox3.Text=="")
            {
                MessageBox.Show("Entre com o preço do produto", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBox3.Focus();
                return true;
            }

            return false;

        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
}
