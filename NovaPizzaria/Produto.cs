﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;
using iTextSharp.text.pdf;
using System.Globalization;


namespace NovaPizzaria
{
    public class Produto
    {
        

        public string nome { get; set; }
        public String codigoProduto;



        Conexao conexao = new Conexao();
        SQLiteCommand cmd = new SQLiteCommand();
        public String Mensagem;
        //Metodo para Cadastrar uma novo Produto
        public void CadastrarProduto(String codigo, String nome, String unidade,decimal valor,String descricao,int id_categoria)
        {
            String valorPonto = valor.ToString(CultureInfo.CreateSpecificCulture("en-US"));
            if (Convert.ToInt32(this.existeComCodigo(codigo)) != 0)
            {
                this.movePraFrente(codigo);
            }
            cmd.CommandText = "INSERT INTO produto (codigo,nome,unidade,valor,descricao,id_categoria) VALUES (@codigo,@nome,@unidade,@valor,@descricao,@id_categoria)";
            string formatoValor = $"{valor:F2}";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            cmd.Parameters.AddWithValue("@nome", nome);
            cmd.Parameters.AddWithValue("@unidade", unidade);
            cmd.Parameters.AddWithValue("@valor", valorPonto);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            cmd.Parameters.AddWithValue("@id_categoria", id_categoria);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Cadastrado com sucesso", "Mensagem:", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }


        //Lista os produtos para a listview
        public SQLiteDataReader ListaProdutosListView()
        {
            Conexao conexao = new Conexao();
            conexao.conectar();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand();
                String produtosSelect = "Select p.id,p.codigo,p.nome,p.valor from categoria c inner join produto p on p.id_categoria = c.id order by p.codigo;";
                SQLiteCommand cmdProd = new SQLiteCommand(produtosSelect, conexao.conectar());
                SQLiteDataReader sQLiteDataReader = cmdProd.ExecuteReader();
                return sQLiteDataReader;
            }
            catch (SQLiteException erro)
            {
                throw erro;
            }
            
        }
        public SQLiteDataReader ListaProdutosFiltro(String teclado)
        {
            Conexao conexao = new Conexao();
            conexao.conectar();
            try
            {
                SQLiteCommand cmd = new SQLiteCommand();
                String produtosSelect = "Select p.id,p.codigo,p.nome,p.valor from categoria c inner join produto p on p.id_categoria = c.id where p.nome like '%"+teclado+"%' or p.codigo = '"+teclado+"' order by p.codigo; ";
                SQLiteCommand cmdProd = new SQLiteCommand(produtosSelect, conexao.conectar());
                SQLiteDataReader sQLiteDataReader = cmdProd.ExecuteReader();
                return sQLiteDataReader;
            }
            catch (SQLiteException erro)
            {
                throw erro;
            }
            conexao.desconectar();
        }

        public void movePraFrente(String codigo)
        {
            cmd.CommandText = "update produto set codigo = codigo + 1 where codigo >= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public void movePraTraz(String codigo)
        {
            cmd.CommandText = "update produto set codigo = codigo - 1 where codigo >= @codigo";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public String existeComCodigo(String codigo)
        {
            Conexao conexao = new Conexao();
            String selectQuantCodigo = "Select count(*) from produto where codigo = " + codigo;

            try
            {
                
                SQLiteCommand cmdQuant = new SQLiteCommand(selectQuantCodigo, conexao.conectar());
                string quant = Convert.ToString(cmdQuant.ExecuteScalar());
                return (quant);
            }
            catch (SQLiteException erro)
            {
                throw erro;
            }
        }

        // Lista os produtos para a datagridview
        public DataTable ListaProdutos()
        {
            Conexao conexao = new Conexao();
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "Select p.id,p.codigo,p.nome,p.unidade,p.valor,p.descricao, c.nome as categoria from categoria c inner join produto p on p.id_categoria = c.id order by p.codigo; ";
            try
            {
                SQLiteDataAdapter obj = new SQLiteDataAdapter(cmd.CommandText, conexao.conectar());
                DataTable dtlista = new DataTable();
                obj.Fill(dtlista);
                conexao.desconectar();
                return dtlista;

            }
            catch (SQLiteException erro)
            {
                throw erro;
            }

        }

        //Metodo para Mostrar os Produtos no Combobox
        public DataSet Lista_Produtos()
        {
            Conexao conexao = new Conexao();
            DataSet ds = new DataSet();

            try
            {

                using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT * FROM produto", conexao.conectar()))
                {
                    da.Fill(ds, "Produto");
                }

            }
            catch (Exception erro)
            {
                throw erro;
            }
            //retorna DataSet
            conexao.desconectar();
            return ds;

        }

        public DataTable buscaProduto()
        {
            Conexao conexao = new Conexao();
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "Select valor, nome from produto";
            try
            {
                SQLiteDataAdapter obj = new SQLiteDataAdapter(cmd.CommandText, conexao.conectar());
                DataTable dtlista = new DataTable();
                obj.Fill(dtlista);
                conexao.desconectar();
                //DataView dataView = new DataView(dtlista);                

                return dtlista;

            }
            catch (SQLiteException erro)
            {
                throw erro;
            }

        }


        //Metodo para Excluir 
        public void ExcluirProduto(int idproduto, int op, int cod)
        {
            cmd.CommandText = "DELETE FROM produto WHERE id=@idproduto";
            cmd.Parameters.AddWithValue("@idproduto", idproduto);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Excluído com sucesso!","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
                if (op == 1)
                {
                    movePraTraz(Convert.ToString(cod));
                }
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }

        //Metodo para Editar Produtos
        public void Editar(String nome, String unidade,decimal valor,String descricao, int codigo, int id_cat, int id)
        {
            String valorPonto = valor.ToString(CultureInfo.CreateSpecificCulture("en-US"));

            if (Convert.ToInt32(this.existeComCodigo(codigo.ToString())) != 0 && codigo.ToString() != codigoProduto)
            {
                this.movePraFrente(codigo.ToString());
            }
            cmd.CommandText = "update produto set codigo = @codigo, nome=@nome,unidade=@unidade,valor=@valor,descricao=@descricao,id_categoria=@id_cat where id=@id";
            cmd.Parameters.AddWithValue("@codigo", codigo);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@nome", nome);
            cmd.Parameters.AddWithValue("@unidade", unidade);
            cmd.Parameters.AddWithValue("@valor", valorPonto);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            cmd.Parameters.AddWithValue("@id_cat", id_cat);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Alterado com Sucesso","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }

        //Mostrar os produtos cadastrados
        public DataSet Itens()
        {
            Conexao conexao = new Conexao();
            DataSet ds = new DataSet();

            try
            {


                using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT id, nome, valor FROM produto", conexao.conectar()))
                {
                    da.Fill(ds, "Produto");
                }

            }
            catch (Exception erro)
            {
                throw erro;
            }
            //retorna DataSet

            return ds;

        }
        
        
    }
}
