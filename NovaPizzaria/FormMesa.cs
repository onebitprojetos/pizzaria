﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using System.Runtime.InteropServices;


namespace NovaPizzaria
{
    public partial class FormMesa : Form
    {

        public int numMesa;
        public String nomeCli;
        public DataGridView d1;
        public PictureBox numeroPictureBox;//picturebox passada ao clicar na mesa
        public Boolean estaAberta=false; //flag que verifica se a mesa está aberta(true) ou fechada(false)
        public Bitmap img1 = new Bitmap("D:\\Documentos\\Pizzaria\\NovaPizzaria\\Imagens e Icones\\disponivel1.png");
        public Bitmap img2 = new Bitmap("D:\\Documentos\\Pizzaria\\NovaPizzaria\\Imagens e Icones\\indisponivel1.png");
        /*    public FormMesa()
          {
               InitializeComponent();

               IntPtr hMenu = GetSystemMenu(this.Handle, false);
               int MenuCount = GetMenuItemCount(hMenu) - 1;
               RemoveMenu(hMenu, MenuCount, MF_BYCOMMAND);
               MostrarProdutos();//inicializa a list view

           }*/

        public FormMesa()
        {
            InitializeComponent();
            MostrarProdutos();
        }
        private void limparTodosProd()
        {
            foreach (ListViewItem item in todosProd.Items)
            {
                todosProd.Items.Remove(item);
            }
        }

        private void MostrarProdutos()
        {
            busca.Text = "";
            if (todosProd.Items.Count >= 0)
            {
                limparTodosProd();
            }
            Produto prod = new Produto();
            SQLiteDataReader sQLiteDataReader = prod.ListaProdutosListView();//recebe os produtos do bd, no formato SQLiteDataReader
            while (sQLiteDataReader.Read())// itera-se os produtos para jogá-los na listview (cada iteraçao lança uma linha)
            {
                ListViewItem lvi = new ListViewItem(((IDataRecord)sQLiteDataReader)[0].ToString());//pucha o id e joga dentro de um listviewitem
                lvi.SubItems.Add(((IDataRecord)sQLiteDataReader)[1].ToString());//codigo
                lvi.SubItems.Add(((IDataRecord)sQLiteDataReader)[2].ToString());//o nome
                lvi.SubItems.Add($"{Convert.ToDecimal((((IDataRecord)sQLiteDataReader)[3].ToString())):F2}");//o preço
                todosProd.Items.Add(lvi);//o listviewitem é jogado dentro da listview do form
            }


        }

        const int MF_BYCOMMAND = 0X400;
        [DllImport("user32")]
        static extern int RemoveMenu(IntPtr hMenu, int nPosition, int wFlags);
        [DllImport("user32")]
        static extern IntPtr GetSystemMenu(IntPtr hWnd, bool bRevert);
        [DllImport("user32")]
        static extern int GetMenuItemCount(IntPtr hWnd);

        //Botão Novo Pedido
        private void Button1_Click(object sender, EventArgs e)
        {
            

            if (nomeCliente.Text == "")
            {
                MessageBox.Show("Insira o nome do cliente", "Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Object[] objeto= new Object[3];
                objeto[0] = "Mesa "+numMesa;
                objeto[1] = nomeCliente.Text;
                objeto[2] = img2;
                d1.CurrentRow.SetValues(objeto);


                //this.nomeCli = nomeCliente;
                this.label1.Visible = true;
                this.textBoxMesaPedido.Visible = true;
                textBoxMesaPedido.Text = Convert.ToString(numMesa);
                Pedido p = new Pedido();
                estaAberta = true;// a mesa esta em uso
                
                //_form1.fechado(numeroPictureBox);
                //.Text = nomeCliente.Text;
                //nomeCli.Visible = true;
                Pedido.Visible = true;
                textBoxMesaPedido.Enabled = false;
                textBoxMesaPedido.Visible = true;
                label1.Visible = true;
                nomeCliente.Enabled = false;
                novoPedido.Enabled = false;
                idPedido.Text = Convert.ToString(p.ObterItem());
                
                totalPed.Text = "0,00";
            }
        }

        // ao clicar no botão cancelar
        private void buttonCancelar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja cancelar a venda?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==  DialogResult.Yes)
            {

                //_form1.aberto(numeroPictureBox);
                
                estaAberta = false;
                Object[] objeto = new Object[3];
                objeto[0] = "Mesa " + numMesa;
                objeto[1] = "Disponível";
                objeto[2] = img1;
                d1.Rows[numMesa - 1].SetValues(objeto);
                this.Close();
            }

        }
        //groupBox da tela de pedidos
        private void FormNovoPedido_Load(object sender, EventArgs e)
        {
            this.label1.Visible = false;
            this.textBoxMesaPedido.Visible = false;
            this.Text = "Mesa " + this.numMesa;
            nomeCliente.Focus();

        }

        private ListViewItem jaExisteNoPedido(String cod)
        {
            for (int i =0; i < itensPed.Items.Count; i++)
            {
                var item = itensPed.Items[i];
                if (item.SubItems[1].Text.Equals(cod))
                {
                    return item;
                }
            }
            return null;
        }

        //Ao clicar no botão Adicionar
        private void adicionar_Click(object sender, EventArgs e)
        {
            
            if (todosProd.SelectedItems.Count == 0)//verifica se há itens selecionados
                return;
            
            for (int i = 0; i < todosProd.SelectedItems.Count; i++)
            {
                ListViewItem lvi = new ListViewItem(todosProd.SelectedItems[i].Text);
                lvi.SubItems.Add(todosProd.SelectedItems[i].SubItems[1].Text);
                lvi.SubItems.Add(todosProd.SelectedItems[i].SubItems[2].Text);
                lvi.SubItems.Add(todosProd.SelectedItems[i].SubItems[3].Text);

                ListViewItem aux = jaExisteNoPedido(todosProd.SelectedItems[i].SubItems[1].Text);

                if (aux==null)
                {
                    lvi.SubItems.Add("1");
                    itensPed.Items.Add(lvi);//os itens da linha selecionada são jogados dentro de um listviewitem, que depois é jogado na listview de itens do pedido
                }
                else
                {
                    aux.SubItems[4].Text = (Convert.ToInt32(aux.SubItems[4].Text)+1).ToString();
                }
                
                
                totalPed.Text = ($"{Convert.ToDouble(Convert.ToDouble(totalPed.Text) + Convert.ToDouble(todosProd.SelectedItems[i].SubItems[3].Text)):F2}").ToString();
            }

        }

        //Ao clicar no botão remover
        private void remover_Click(object sender, EventArgs e)
        {
            
            if (itensPed.SelectedItems.Count == 0)//verifica se há item selecionado
                return;

            if (MessageBox.Show("Deseja Remover o(s) item(s) Selecionado(s)?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                for (int i = itensPed.SelectedItems.Count-1; i >=0 ; i--)
                {
                    ListViewItem li = itensPed.SelectedItems[i];
                    totalPed.Text = ($"{Convert.ToDouble((Convert.ToDouble(totalPed.Text) - Convert.ToDouble(itensPed.SelectedItems[i].SubItems[3].Text)).ToString()):F2}").ToString();//atualiza o campo total, mantendo o formato de duas cass decimais

                    if (li.SubItems[4].Text.Equals("1"))
                    {
                        itensPed.Items.Remove(li);// remove o item
                    }
                    else
                    {
                        li.SubItems[4].Text = (Convert.ToInt32(li.SubItems[4].Text)-1).ToString();
                    }
                    
                    
                }
               

            }
        }

        //Ao clicar no botão Ocultar Mesa
        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();//esconde o form, mas não o fecha
        }

        //Ao clicar no botão prosseguir para forma de pagamento
        private void button2_Click_1(object sender, EventArgs e)
        {

            if (totalPed.Text == "0,00" || totalPed.Text == "0")// verifica se o total é zero, se for significa que não foram inserido produtos ao pedido
            {
                MessageBox.Show("Nenhum Item Inserido","Aviso!",MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                FormFinalizaCompra fc = new FormFinalizaCompra(totalPed.Text)
                {
                    produtos = itensPed, //passa a lista de produtos
                    nome = nomeCliente.Text,
                    numeroMesa = numMesa,
                    id_Pedido = idPedido.Text,
                    formMesa = this,// passa o form da mesa em questão
                    Pb = numeroPictureBox,
                    //lb = nomeCli
                };
                this.Enabled = false;//deixa este form aberto, porém impossível de editar
                fc.Show();//exibe o form finaliza compra
            }

        }

        //groupBox dos pedidos
        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void nomeCliente_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                Button1_Click(sender, e);
            }
        }

        private void todosProd_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void totalPed_TextChanged(object sender, EventArgs e)
        {

        }

        private void loadProd()
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (busca.Text != "")
            {
                limparTodosProd();
                Produto prod = new Produto();
                SQLiteDataReader sQLiteDataReader = prod.ListaProdutosFiltro(busca.Text);//recebe os produtos do bd, no formato SQLiteDataReader
                while (sQLiteDataReader.Read())// itera-se os produtos para jogá-los na listview (cada iteraçao lança uma linha)
                {
                    ListViewItem lvi = new ListViewItem(((IDataRecord)sQLiteDataReader)[0].ToString());//pucha o id e joga dentro de um listviewitem
                    lvi.SubItems.Add(((IDataRecord)sQLiteDataReader)[1].ToString());//codigo
                    lvi.SubItems.Add(((IDataRecord)sQLiteDataReader)[2].ToString());//o nome
                    lvi.SubItems.Add($"{Convert.ToDecimal((((IDataRecord)sQLiteDataReader)[3].ToString())):F2}");//o preço
                    todosProd.Items.Add(lvi);//o listviewitem é jogado dentro da listview do form
                }
            }
            else
            {
                MostrarProdutos();
            }
        }

        private void Atualizar_Click(object sender, EventArgs e)
        {
            MostrarProdutos();
        }
    }
}
