﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormListarCategorias : Form
    {
        public FormListarCategorias()
        {
            InitializeComponent();
            listar();
        }

        private void FormEditarCategorias_Load(object sender, EventArgs e)
        {
            
            dataGridView1.BorderStyle = BorderStyle.None;
            //dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            //dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            //dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;

            dataGridView1.EnableHeadersVisualStyles = false;
            //dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            //dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            //dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
        }
        //CHAMAR O MÉTODO LISTAR NA GRID
        public void listar()
        {

            Categoria category = new Categoria();
            dataGridView1.DataSource = category.list();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            else
            {

                if (MessageBox.Show("Ao excluir a categoria, todos os produtos cadastrados nela serão excluídos! Deseja continuar?", "Mensagem:", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Categoria category = new Categoria();
                    int a;
                    a = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    category.ExcluirCategoria(a);
                    if (dataGridView1.RowCount > 0)
                        dataGridView1.Rows[0].Selected = true;
                    listar();
                }
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            else
            {
                int id;
                String nome, descricao;
                id = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                nome = dataGridView1.CurrentRow.Cells[1].Value.ToString();
                descricao = dataGridView1.CurrentRow.Cells[2].Value.ToString();
                FormEditarCategoria novaForm = new FormEditarCategoria(Convert.ToString(id), nome, descricao);
                novaForm.ShowDialog(this);
            }
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
