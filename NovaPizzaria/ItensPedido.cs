﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public class ItensPedido
    {

        Conexao conexao = new Conexao();
        SQLiteCommand cmd = new SQLiteCommand();
        public String Mensagem;
        //Metodo para Cadastrar uma novo Produto
        public void CadastrarItensPedido(int id_produto, int id_pedido, int quantidade)
        {
            try
            {
                cmd.CommandText = "INSERT INTO itens_pedido(id_produto,id_pedido, quantidade) VALUES (@id_produto,@id_pedido,@quantidade)";

            cmd.Parameters.AddWithValue("@id_pedido", id_pedido);
            cmd.Parameters.AddWithValue("@id_produto", id_produto);
            cmd.Parameters.AddWithValue("@quantidade", quantidade);
            
            
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                
            }
            catch (SQLiteException E)
            {
                MessageBox.Show("Erro", "Mensagem:", MessageBoxButtons.OK, MessageBoxIcon.Warning);
               
            }
        }

        //Listar Itens Inseridos
        public DataTable ListaItensInseridos(int id_pedido)
        {
            
            Conexao conexao = new Conexao();
            SQLiteCommand cmd = new SQLiteCommand();
            
            cmd.CommandText = "select p.id,c.nome,c.valor,p.quantidade from itens_pedido p inner join produto c on p.id_produto = c.id  where p.id_pedido=('" + id_pedido+ "') order by p.id desc";
            
            try
            {
                SQLiteDataAdapter obj = new SQLiteDataAdapter(cmd.CommandText, conexao.conectar());
                DataTable dtlista = new DataTable();
                obj.Fill(dtlista);
                conexao.desconectar();
                return dtlista;

            }
            catch (SQLiteException erro)
            {
                throw erro;
            }

        }
        public void ExcluirItem(int id)
        {
            cmd.CommandText = "DELETE FROM itens_pedido WHERE id=@id";
            cmd.Parameters.AddWithValue("@id", id);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Excluído com sucesso!","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }






    }
}
