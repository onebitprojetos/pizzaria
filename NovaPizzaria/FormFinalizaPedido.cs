﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormFinalizaPedido : Form
    {
        public FormFinalizaPedido(String idPedido, String cliente)
        {
            InitializeComponent();
            textBoxCodPedido.Text = idPedido;
            textBoxNomeCliente.Text = cliente;
            MostrarProdutos();
        }
        //CHAMAR O MÉTODO LISTAR NA GRID
       

        private void MostrarProdutos()
        {
            Produto cat = new Produto();


            /* comboBox1.DataSource = new BindingSource(cat.Itens(), "Produto");
           
            comboBox1.ValueMember = "valor";
            comboBox1.DisplayMember = "nome";
            comboBox1.SelectedIndex = 0;


            textBox1.Text = comboBox1.SelectedValue.ToString();
            */
            DataTable dadosProduto = new DataTable();
            dadosProduto =  cat.buscaProduto();
            DataView dados = new DataView(dadosProduto);
            dados.RowFilter = "nome like '%" + comboBox1.Text + "%'";
            comboBox1.DataSource = dados;
            comboBox1.ValueMember = "valor";
            comboBox1.DisplayMember = "nome";
            comboBox1.Refresh();
            textBox1.Text = comboBox1.SelectedValue.ToString();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Produto cat = new Produto();
            textBox1.Text = comboBox1.SelectedValue.ToString();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ItensPedido itens = new ItensPedido();
            //itens.CadastrarProduto(Convert.ToInt32( textBoxCodPedido.Text),Convert.ToInt32( textBox4.Text),Convert.ToDouble(textBox1.Text));
            int id_pedido = Convert.ToInt32( textBoxCodPedido.Text);
            string nome_produto = comboBox1.Text;
            MessageBox.Show(nome_produto);
            int quantidade = Convert.ToInt32(textBox4.Text);
            Double valor = Convert.ToDouble(textBox1.Text);
            Double subtotal = Convert.ToDouble(quantidade*valor);

            itens.CadastrarItensPedido(id_pedido, nome_produto, quantidade, valor, subtotal);
            dataGridView1.DataSource =  itens.ListaItensPedido(Convert.ToString(id_pedido));
        }

        //carregamento da tela
        private void FormFinalizaPedido_Load(object sender, EventArgs e)
        {
            ItensPedido itens = new ItensPedido();
            int id_pedido = Convert.ToInt32(textBoxCodPedido.Text);
            dataGridView1.DataSource = itens.ListaItensPedido(Convert.ToString(id_pedido));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido");
            else
            {
                if (MessageBox.Show("Tem certeza que deseja Excluir?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ItensPedido  p = new ItensPedido();
                    int a;
                    a = int.Parse(dataGridView1.CurrentRow.Cells[0].Value.ToString());
                    p.ExcluirPedido(a);
                    if (dataGridView1.RowCount > 0)
                        dataGridView1.Rows[0].Selected = true;
                    
                    ItensPedido itens = new ItensPedido();
                    int id_pedido = Convert.ToInt32(textBoxCodPedido.Text);
                    dataGridView1.DataSource = itens.ListaItensPedido(Convert.ToString(id_pedido));

                }
                else
                {
                    MessageBox.Show("Exclusão Cancelada com Sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
