﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Windows.Forms;


namespace NovaPizzaria
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
        }

        private void buttonCadCategoria_Click(object sender, EventArgs e)
        {
            FormCategoria categoria = new FormCategoria();
            categoria.ShowDialog();
        }

        private void categoriaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormCategoria categoria = new FormCategoria();
            categoria.ShowDialog();
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormProduto produto = new FormProduto();
            produto.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormProduto produto = new FormProduto();
            if (Convert.ToInt32(produto.existeCategoria()) >= 1)
            {
                produto.ShowDialog();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormListarCategorias editar = new FormListarCategorias();
            editar.ShowDialog();
        }

        private void categoriaToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormListarCategorias editar = new FormListarCategorias();
            editar.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormListaProdutos l = new FormListaProdutos();
            l.ShowDialog();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            label2.Text = (DateTime.Now.ToString("HH:mm:ss"));
            label1.Text = DateTime.Now.ToShortDateString();
            
        } 
        
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void produtosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormListaProdutos l = new FormListaProdutos();
            l.ShowDialog();
        }

        private void sairToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (formDeMesas != null)
            {
                formDeMesas.Show();
            }
            else
            {
                formDeMesas = new FormMenuMesas();
                formDeMesas.Show();
            }
        }

        public FormMenuMesas formDeMesas;

        private void buttonNovoPedido_Click(object sender, EventArgs e)
        {
            if (formDeMesas != null)
            {
                formDeMesas.Show();
            }
            else
            {
                formDeMesas = new FormMenuMesas();
                formDeMesas.Show();
            }
            
            
            


        }

        private void button4_Click(object sender, EventArgs e)
        {
           
        }

        private void pediddosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void pedidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            FormListarPedido lp = new FormListarPedido();
            lp.ShowDialog();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }



        private void pedidosToolStripMenuItem_Click_2(object sender, EventArgs e)
        {
            FormListarPedido lp = new FormListarPedido();
            lp.ShowDialog();
        }
    }
}
