﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormListarPedido : Form
    {
        public FormListarPedido()
        {
            InitializeComponent();
            listar();
        }
        private void FormListaPedidos_Load(object sender, EventArgs e)
        {
            dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.FromArgb(238, 239, 249);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.DarkTurquoise;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.WhiteSmoke;
            dataGridView1.BackgroundColor = Color.White;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
            dataGridView1.EnableHeadersVisualStyles = false;
            dataGridView1.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.FromArgb(20, 25, 72);
            dataGridView1.ColumnHeadersDefaultCellStyle.ForeColor = Color.White;
            dataGridView1.Columns[4].DefaultCellStyle.Format = "C";
            dataGridView1.Columns[5].DefaultCellStyle.Format = "C";
            dataGridView1.Columns[6].DefaultCellStyle.Format = "C";

        }
        public void listar()
        {
            Pedido list = new Pedido();
            
            dataGridView1.DataSource = list.ListaPedido();
            dataGridView1.Columns[0].HeaderText = "Código";
            dataGridView1.Columns[1].HeaderText = "Data";
            dataGridView1.Columns[2].HeaderText = "Cliente";
            dataGridView1.Columns[3].HeaderText = "Mesa";
            dataGridView1.Columns[4].HeaderText = "SubTotal";
            dataGridView1.Columns[5].HeaderText = "Desconto";
            dataGridView1.Columns[6].HeaderText = "Total";

            dataGridView1.Columns[0].Visible = false;
            dataGridView1.Columns[4].DefaultCellStyle.Format = "C2";
            dataGridView1.Columns[5].DefaultCellStyle.Format = "C2";
            dataGridView1.Columns[6].DefaultCellStyle.Format = "C2";

            dataGridView1.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            totalDia.Text = Convert.ToString(($"{Convert.ToDouble(list.GetTotalDia()):F2}"));

            dataGridView1.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dataGridView1.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.SingleHorizontal;
        }



        private void button2_Click(object sender, EventArgs e)
        {
            
        }
        private void FormListarPedido_Load(object sender, EventArgs e)
        {

        }
 
        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1Voltar_Click(object sender, EventArgs e)
        {
            PDFiTextSharp.GerarPdf novo = new PDFiTextSharp.GerarPdf();
            novo.geradorPdf(dataGridView1.CurrentRow.Cells[0].Value.ToString(), "0", "0");
        }
    }
}
