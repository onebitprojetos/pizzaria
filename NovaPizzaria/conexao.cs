﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace NovaPizzaria
{
    public class Conexao
    {
        SQLiteConnection con = new SQLiteConnection();
        //Construtor
        public Conexao()
        {
            con.ConnectionString = "Data Source=pizzaria.db";
        }

        public SQLiteConnection conectar()
        {
            if (con.State == System.Data.ConnectionState.Closed)
            {
                con.Open();
            }
            return con;
        }
        public void desconectar()
        {
            if (con.State == System.Data.ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}
