﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.Data;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public class Categoria
    {
        Conexao conexao = new Conexao();
        SQLiteCommand cmd = new SQLiteCommand();
        public String Mensagem;
        //Metodo para Cadastrar uma nova Categoria
        public void Cadastrar(String nome, String descricao)
        {
            cmd.CommandText = "INSERT INTO categoria (nome,descricao) VALUES (@nome,@descricao)";

            cmd.Parameters.AddWithValue("@nome", nome);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Cadastrado com sucesso","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }


        //Metodo para Listar as Categorias Cadastradas
        public DataTable list()
        {
            Conexao conexao = new Conexao();
            SQLiteCommand cmd = new SQLiteCommand();
            cmd.CommandText = "SELECT  * FROM categoria";
            try
            {
                SQLiteDataAdapter obj = new SQLiteDataAdapter(cmd.CommandText, conexao.conectar());
                DataTable dtlista = new DataTable();
                obj.Fill(dtlista);
                conexao.desconectar();
                return dtlista;

            }
            catch (SQLiteException erro)
            {
                throw erro;
            }

        }


        //Metodo para Excluir Categoria
        public void ExcluirCategoria(int idcategoria)
        {
            cmd.CommandText = "DELETE FROM categoria WHERE id=@idcategoria";
            cmd.Parameters.AddWithValue("@idcategoria", idcategoria);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Excluído com sucesso!","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException E)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }

        //Metodo para Editar Categoria
        public void Editar(int id, String nome,String descricao)
        {
            cmd.CommandText = "update categoria set nome=@nome,descricao=@descricao where id=@id";
            cmd.Parameters.AddWithValue("@id", id);
            cmd.Parameters.AddWithValue("@nome", nome);
            cmd.Parameters.AddWithValue("@descricao", descricao);
            try
            {
                cmd.Connection = conexao.conectar();
                cmd.ExecuteNonQuery();
                conexao.desconectar();
                MessageBox.Show("Alterado com Sucesso","Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (SQLiteException e)
            {
                this.Mensagem = "Erro ao tentar se conectar com o Banco de Dados";
            }
        }

        //Mostrar as Categorias cadastradas
        public DataSet Itens()
        {
            Conexao conexao = new Conexao();
            DataSet ds = new DataSet();

            try
            {
                
                using (SQLiteDataAdapter da = new SQLiteDataAdapter("SELECT id, nome FROM categoria", conexao.conectar()))
                {
                    da.Fill(ds, "Categoria");
                }

            }
            catch (Exception erro)
            {
                throw erro;
            }
            //retorna DataSet

            return ds;

        }
    }
}
