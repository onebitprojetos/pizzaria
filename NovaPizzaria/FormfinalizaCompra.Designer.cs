﻿namespace NovaPizzaria
{
    partial class FormFinalizaCompra
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1Voltar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.finalizar = new System.Windows.Forms.Button();
            this.troco = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.dinheiro = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.total = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.desconto = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.subTotal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1Voltar
            // 
            this.button1Voltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1Voltar.Image = global::NovaPizzaria.Properties.Resources._1486564399_close_81512;
            this.button1Voltar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1Voltar.Location = new System.Drawing.Point(239, 330);
            this.button1Voltar.Name = "button1Voltar";
            this.button1Voltar.Size = new System.Drawing.Size(108, 44);
            this.button1Voltar.TabIndex = 4;
            this.button1Voltar.Text = "Cancelar";
            this.button1Voltar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1Voltar.UseVisualStyleBackColor = true;
            this.button1Voltar.Click += new System.EventHandler(this.button1Voltar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.label3.Location = new System.Drawing.Point(106, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 31);
            this.label3.TabIndex = 34;
            this.label3.Text = "Pagamento";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.finalizar);
            this.groupBox1.Controls.Add(this.button1Voltar);
            this.groupBox1.Controls.Add(this.troco);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.dinheiro);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.total);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.desconto);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.subTotal);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(12, 73);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(370, 386);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            // 
            // finalizar
            // 
            this.finalizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finalizar.Image = global::NovaPizzaria.Properties.Resources.success_icon_icons_com_52365;
            this.finalizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.finalizar.Location = new System.Drawing.Point(111, 330);
            this.finalizar.Name = "finalizar";
            this.finalizar.Size = new System.Drawing.Size(108, 44);
            this.finalizar.TabIndex = 3;
            this.finalizar.Text = "Finalizar";
            this.finalizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.finalizar.UseVisualStyleBackColor = true;
            this.finalizar.Click += new System.EventHandler(this.finalizar_Click);
            // 
            // troco
            // 
            this.troco.Enabled = false;
            this.troco.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.troco.Location = new System.Drawing.Point(210, 251);
            this.troco.Name = "troco";
            this.troco.Size = new System.Drawing.Size(137, 49);
            this.troco.TabIndex = 29;
            this.troco.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(205, 219);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 29);
            this.label13.TabIndex = 28;
            this.label13.Text = "Troco";
            this.label13.Visible = false;
            // 
            // dinheiro
            // 
            this.dinheiro.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dinheiro.Location = new System.Drawing.Point(210, 163);
            this.dinheiro.Name = "dinheiro";
            this.dinheiro.Size = new System.Drawing.Size(137, 49);
            this.dinheiro.TabIndex = 2;
            this.dinheiro.Visible = false;
            this.dinheiro.TextChanged += new System.EventHandler(this.dinheiro_TextChanged);
            this.dinheiro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dinheiro_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(205, 131);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 29);
            this.label12.TabIndex = 26;
            this.label12.Text = "Dinheiro";
            this.label12.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(34, 219);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 29);
            this.label10.TabIndex = 23;
            this.label10.Text = "Total";
            // 
            // total
            // 
            this.total.Enabled = false;
            this.total.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.total.Location = new System.Drawing.Point(39, 251);
            this.total.Name = "total";
            this.total.Size = new System.Drawing.Size(137, 49);
            this.total.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(34, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 29);
            this.label9.TabIndex = 21;
            this.label9.Text = "Desconto";
            // 
            // desconto
            // 
            this.desconto.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.desconto.Location = new System.Drawing.Point(39, 163);
            this.desconto.Name = "desconto";
            this.desconto.Size = new System.Drawing.Size(101, 49);
            this.desconto.TabIndex = 1;
            this.desconto.TextChanged += new System.EventHandler(this.desconto_TextChanged);
            this.desconto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.desconto_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(34, 41);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 29);
            this.label8.TabIndex = 19;
            this.label8.Text = "SubTotal";
            // 
            // subTotal
            // 
            this.subTotal.Enabled = false;
            this.subTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subTotal.Location = new System.Drawing.Point(39, 73);
            this.subTotal.Name = "subTotal";
            this.subTotal.Size = new System.Drawing.Size(137, 49);
            this.subTotal.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(143, 171);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 37);
            this.label1.TabIndex = 30;
            this.label1.Text = "%";
            // 
            // FormFinalizaCompra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(398, 471);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormFinalizaCompra";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Load += new System.EventHandler(this.finalizaCompra_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1Voltar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox troco;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox dinheiro;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox total;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox desconto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox subTotal;
        private System.Windows.Forms.Button finalizar;
        private System.Windows.Forms.Label label1;
    }
}