﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;


namespace NovaPizzaria
{
    public partial class Mesa01 : Form
    {
        private readonly FormMesas _form1;
        public Mesa01(FormMesas mesas)
        {
            InitializeComponent();
            _form1 = mesas;
            MostrarProdutos();
            AdicionarItens();
        }
        private void MostrarProdutos()
        {
            
                Produto prod = new Produto();

                comboBox1.DataSource = new BindingSource(prod.Lista_Produtos(), "Produto");
                comboBox1.ValueMember = "id";
                comboBox1.DisplayMember = "nome";
                comboBox1.SelectedIndex = 0;

        }

        private void button1_Click(object sender, EventArgs e)
        {
                if(textBoxNomeCliente.Text=="")
                {
                MessageBox.Show("Insira o nome do Cliente");
                }
                else
                {
                int mesa = 1;
                textBoxMesaPedido.Text = Convert.ToString(mesa);
                Pedido pedido = new Pedido();
                pedido.Cadastrar(textBoxMesaPedido.Text, textBoxNomeCliente.Text);
                MessageBox.Show(pedido.Mensagem);

                _form1.fechado();
                Pedido.Visible = true;
                textBoxMesaPedido.Enabled = false;
                textBoxMesaPedido.Visible = true;
                label1.Visible = true;
                textBoxNomeCliente.Enabled = false;
                buttonNovoPedido.Enabled = false;
                textBox1.Text = Convert.ToString(pedido.ObterItem());
            }
                
            


            
        }

        public static bool CamposVazios(Control _ctrl)
        {
            foreach (Control c in _ctrl.Controls)
            {
                if (c is TextBox)
                {
                    if (String.IsNullOrEmpty(((TextBox)c).Text))
                        return true;
                }
                else if (c is ComboBox)
                {
                    if (((ComboBox)c).SelectedValue == null)
                        return true;
                }
                else if (c.HasChildren)
                {
                    if (CamposVazios(c))
                        return true;
                }
            }

            return false;

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            ItensPedido p = new ItensPedido();
            p.CadastrarItensPedido( Convert.ToInt32(comboBox1.SelectedValue),Convert.ToInt32(textBox1.Text),Convert.ToInt32(numericUpDown1.Value));
            listar();
        }
        public void listar()
        {
            
            ItensPedido list = new ItensPedido();
            dataGridView1.DataSource = list.ListaItensInseridos(Convert.ToInt32(textBox1.Text));
            foreach(DataGridViewRow row in dataGridView1.Rows)
            {
                row.Cells["SubTotal"].Value = Convert.ToDecimal(row.Cells["Quantidade"].Value) * Convert.ToDecimal(row.Cells["Valor"].Value);
            }
            textBox2.Text = dataGridView1.Rows.Cast<DataGridViewRow>().Sum(i => Convert.ToDecimal(i.Cells[0].Value ?? 0)).ToString("##.00");



        }
        private void FormNovoPedido_Load(object sender, EventArgs e)
        {

        }
        public void AdicionarItens()
        {
            comboBox2.Items.Add("Dinheiro");
            comboBox2.Items.Add("Cartão de Crédito");
            comboBox2.Items.Add("Cartão de Débito");
        }
        private void button2_Click(object sender, EventArgs e)
        {
            
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            groupBox1.Visible = true;
            subt.Text = textBox2.Text;
        }

        private void total_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void desconto_TextChanged(object sender, EventArgs e)
        {
            if(desconto.Text=="")
            {
                desconto.Text = "0";
            }
            total.Text = Convert.ToString(Convert.ToDouble(subt.Text) - Convert.ToDouble(desconto.Text));
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox2.SelectedIndex ==0)
            {
                label12.Visible = true;
                dinheiro.Visible = true;
                label13.Visible = true;
                troco.Visible = true;
            }
            else
            {
                label12.Visible = false;
                dinheiro.Visible = false;
                label13.Visible = false;
                troco.Visible = false;
            }
      //Executa sua função "if" aqui
                
    }

        private void dinheiro_TextChanged(object sender, EventArgs e)
        {
            if (dinheiro.Text == "")
            {
                dinheiro.Text = "0";
            }
            troco.Text = Convert.ToString(Convert.ToDouble(dinheiro.Text)-Convert.ToDouble(total.Text));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Tem certeza que deseja Finalizar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Pedido p = new Pedido();
                p.FinalizaPedido(Convert.ToDecimal(subt.Text), Convert.ToDecimal(desconto.Text), Convert.ToDecimal(total.Text), comboBox2.Text, Convert.ToInt32(textBox1.Text));
                _form1.aberto();
                this.Close();
            }
            else
            {
                MessageBox.Show("Cancelado!");
            }
                
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.RowCount == 0)
                MessageBox.Show("Nenhum registro inserido");
            else
            {
                if (MessageBox.Show("Tem certeza que deseja Excluir?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    ItensPedido p = new ItensPedido();
                    int a;
                    a = int.Parse(dataGridView1.CurrentRow.Cells[1].Value.ToString());
                    p.ExcluirItem(a);
                    if (dataGridView1.RowCount > 0)
                        dataGridView1.Rows[0].Selected = true;
                    listar();
                }
                else
                {
                    MessageBox.Show("Exclusão Cancelada com Sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
