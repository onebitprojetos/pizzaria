﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormMenuMesas : Form
    {
        private List<FormMesa> listaMesas = new List<FormMesa>();
        private Bitmap img = new Bitmap("D:\\Documentos\\Pizzaria\\NovaPizzaria\\Imagens e Icones\\disponivel1.png");
        public FormMenuMesas()
        {
            InitializeComponent();
            for (int i = 0; i < 22; i++)
            {
                FormMesa fm = new FormMesa();
                //string aaa = fm.ToString();
               // Console.WriteLine(aaa);
                FormMesa fmmm = new FormMesa();
               // fmmm = aaa;
                listaMesas.Add(fm);
            }
        }


        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }





        private void Form1_Load(object sender, EventArgs e)
        {
            Object[] objeto = new Object[3];
            for (int i = 0; i < 22; i++)
            {
                objeto[0] = "Mesa " + (i+1);
                objeto[1] = "Disponível";
                objeto[2] = img;
                dataGridView1.Rows.Add(objeto);
                objeto = new Object[3];
               dataGridView1.Rows[i].Height = 100;
            }

/*
            Object[] objeto = new Object[3];
            objeto[0] = "Mesa 01";
            objeto[1] = "Josimar";
            objeto[2] = img;
            dataGridView1.Rows.Add(objeto);
            objeto = new Object[3];
            objeto[0] = "Mesa 02";
            objeto[1] = "Saulo";
            objeto[2] = img;
            dataGridView1.Rows.Add(objeto);
            DataGridViewRow row = this.dataGridView1.Rows[0];
            row.Height = 100;

            //dataGridView1.DataSource = listaMesas;
            //dataGridView1.Rows.Add(objeto);
            //
            */

        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            int linha = dataGridView1.CurrentRow.Index;
            if (listaMesas[linha].estaAberta == true)
            {
                listaMesas[linha].Show();
            }
            else
            {
                listaMesas[linha] = new FormMesa();
                listaMesas[linha].numMesa = linha + 1;
                listaMesas[linha].d1 = this.dataGridView1; 
                listaMesas[linha].Show();
            }
            FormMesa mesa01 = new FormMesa();


        }

        private void button2_Click(object sender, EventArgs e)
        {
            int quant;
            if (textBox1.Text == "")
            {
                quant = 1;
            }
            else
            {
                quant = Convert.ToInt32(textBox1.Text);
            }
            FormMesa fm = new FormMesa();
            listaMesas.Add(fm);
            
            Object[] objeto = new Object[3];
            for (int i = 0; i < quant; i++)
            {
                int pos = dataGridView1.Rows.Count;
                objeto[0] = "Mesa " + (pos+1);
                objeto[1] = "Disponível";
                objeto[2] = img;
                dataGridView1.Rows.Add(objeto);
                objeto = new Object[3];
                dataGridView1.Rows[pos].Height = 100;
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listaMesas[listaMesas.Count - 1].estaAberta == true)
            {
                if(MessageBox.Show("A mesa em questão está aberta, tem certeza de que deseja excluí-la?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)==DialogResult.Yes)
                {
                    listaMesas.RemoveAt(listaMesas.Count - 1);
                    dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 1);
                }
            }
            else
            {
               if(MessageBox.Show("Deseja realmente excluir a mesa?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    listaMesas.RemoveAt(listaMesas.Count - 1);
                    dataGridView1.Rows.RemoveAt(dataGridView1.Rows.Count - 1);
                }
            }
            

        }
    }
}
