﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormFinalizaCompra : Form
    {
        public FormFinalizaCompra()
        {
            InitializeComponent();
        }

        public ListView produtos;

        public String nome;
        public string id_Pedido;
        public int numeroMesa;
        public FormMesa formMesa;
        public PictureBox Pb;
        public Label lb;

        public FormFinalizaCompra(String texto)
        {

            InitializeComponent();
            subTotal.Text = texto;//o campo subtotal recebe o total vindo do form mesa
            total.Text = subTotal.Text;
            this.KeyPreview = true;
        }

        //Ao mexer no campo desconto
        private void desconto_TextChanged(object sender, EventArgs e)
        {
            
            if (desconto.Text == "")
            {
                total.Text = Convert.ToString($"{(Convert.ToDouble(subTotal.Text)):F2}");
                if (troco.Text != "")
                    troco.Text = Convert.ToString($"{Convert.ToDouble(Convert.ToDouble(dinheiro.Text) - Convert.ToDouble(total.Text)):F2}");
            }
            else
            {
                total.Text = Convert.ToString($"{(Convert.ToDouble(subTotal.Text) * (1-(Convert.ToDouble(desconto.Text) / 100))):F2}");
                if (troco.Text != "")
                    troco.Text = Convert.ToString($"{Convert.ToDouble(Convert.ToDouble(dinheiro.Text) - Convert.ToDouble(total.Text)):F2}");
            }
            
        }
        //Ao mexer no campo dinheiro
        private void dinheiro_TextChanged(object sender, EventArgs e)
        {
            if(dinheiro.Text == "")
            {
                troco.Text = "";
            }
            else
            {   
                troco.Text = Convert.ToString($"{Convert.ToDouble(Convert.ToDouble(dinheiro.Text) - Convert.ToDouble(total.Text)):F2}");
            }
            
        }
        //botão finalizar
        private void finalizar_Click(object sender, EventArgs e)
        {
            if (dinheiro.Text == "" || desconto.Text == "")
            {
                MessageBox.Show("Existem campos vazios!!!", "Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
            else
            {
                if (Convert.ToDouble(dinheiro.Text) < Convert.ToDouble(total.Text))//verifica se o valor pago e menor do que o total
                {
                    MessageBox.Show("O valor pago é menor do que o total!!!", "Mensagem:",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                }
                else
                {
                    if (MessageBox.Show("Tem certeza que deseja Finalizar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (desconto.Text == "")
                        {
                            desconto.Text = "0";
                        }

                        insereBD();// chama a função que insere os dados no BD
                        //lb.Text = "";
                        //lb.Visible = false;
                        if (MessageBox.Show("Deseja imprimir o comprovante?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            //Cria a nota fiscal
                            PDFiTextSharp.GerarPdf novo = new PDFiTextSharp.GerarPdf();
                            novo.geradorPdf(id_Pedido, dinheiro.Text, troco.Text);
                        }
                        formMesa.Close();//fecha o form da mesa
                        formMesa.estaAberta = false;//a mesa está como aberta

                        Object[] objeto = new Object[3];
                        objeto[0] = "Mesa " + formMesa.numMesa;
                        objeto[1] = "Disponível";
                        objeto[2] = formMesa.img1;
                        formMesa.d1.Rows[formMesa.numMesa - 1].SetValues(objeto);
                        this.Close(); //fecha este form

                    }

                }
            }
            
        }
        
        public void insereBD()
        {
            
            Pedido p = new Pedido();
            p.Cadastrar(numeroMesa.ToString(), nome);//insere alguns dados iniciais
            id_Pedido = Convert.ToString(p.ObterItem());//obtem-se o id da entrada inicial 
            ItensPedido ip = new ItensPedido();
            foreach (ListViewItem lvi in this.produtos.Items)//a listviewitem com os produtos do pedido é percorrida 
            {
                ip.CadastrarItensPedido(Convert.ToInt32(lvi.SubItems[0].Text), Convert.ToInt32(id_Pedido), Convert.ToInt32(lvi.SubItems[4].Text));// e jogada dentro do banco, utilizando-se o id obtido
            }
            p.FinalizaPedido(Convert.ToDecimal(subTotal.Text), Convert.ToDecimal(desconto.Text), Convert.ToDecimal(total.Text), Convert.ToInt32(id_Pedido));//o restante das informações sobre o pedido são inseridas
            //mesa.aberto(Pb);//a cor da mesa muda para verde
            
        }

        //botão voltar
        private void button1Voltar_Click(object sender, EventArgs e)
        {
                formMesa.Enabled = true;//o form de mesa torna-se editável
                formMesa.Show();//o form da mesa é aberto
                this.Close();// e o form finaliza pedido é fechado
            
        }

        private void finalizaCompra_Load(object sender, EventArgs e)
        {
            desconto.Text = "5,00";
            dinheiro.Text = total.Text;
            dinheiro.Focus();

        }

        private void desconto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsPunctuation(e.KeyChar) && !char.IsControl(e.KeyChar) || e.KeyChar == (char)46)
            {
                e.Handled = true;

            }
        }

        private void dinheiro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar) && !char.IsPunctuation(e.KeyChar) && !char.IsControl(e.KeyChar) || e.KeyChar == (char)46)
            {
                e.Handled = true;

            }
        }
    }
}
