﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NovaPizzaria
{
    public partial class FormEditarCategoria : Form
    {
        public FormEditarCategoria(String codigo,String nome,String descricao)
        {
            InitializeComponent();
            textBoxNomeCategoria.Text = nome;
            textBoxDescCategoria.Text = descricao;
            idcategoria.Text = codigo;
        }

        private void EditarCategoria_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CamposVazios(this))
            {
            }
            else
            {
                if (MessageBox.Show("Tem certeza que deseja Editar?", "Pergunta", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Categoria category = new Categoria();
                    category.Editar(Convert.ToInt16(idcategoria.Text), textBoxNomeCategoria.Text, textBoxDescCategoria.Text);
                    ((FormListarCategorias)Owner).listar();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Edição Cancelada com Sucesso", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public bool CamposVazios(Control _ctrl)
        {
            if (textBoxNomeCategoria.Text == "")
            {
                MessageBox.Show("Entre com o nome da Categoria.", "Aviso!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                textBoxNomeCategoria.Focus();
                return true;
            }
            return false;

        }

        private void textBoxDescCategoria_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
